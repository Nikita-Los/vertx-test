package handler.medox;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.RoutingContext;
import service.ParsingService;
import verticle.HttpServerVerticle;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LoginHandlerToMedox implements Handler<RoutingContext> {

    private ParsingService parsingService = new ParsingService();

    @Override
    public void handle(RoutingContext routingContext) {
        String password = routingContext.getBodyAsJson().getString("password");
        String username = routingContext.getBodyAsJson().getString("username");
        JsonArray params = routingContext.getBodyAsJson().getJsonArray("param");
        List<String> listParams = new ArrayList<>();
        for (Object list : params.getList()) {
            listParams.add(((HashMap<String, String>) list).get("name"));
        }

        try {
            URL url = new URL(HttpServerVerticle.medoxUrl + "/medoxweb/Logon.do");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            String urlParameters = "username=" + username + "&password=" + password + "&action=LOGIN";
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            String cookieString = con.getHeaderField("Set-Cookie");

            StringBuilder response = parsingService.parse(con, listParams, cookieString);

            routingContext.response().end(String.valueOf(response));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
