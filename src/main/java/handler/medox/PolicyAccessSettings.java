package handler.medox;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import service.GETRequestToApiMedox;
import verticle.HttpServerVerticle;

import java.net.MalformedURLException;
import java.net.URL;

public class PolicyAccessSettings implements Handler<RoutingContext> {

    @Override
    public void handle(RoutingContext routingContext) {
        try {
            String requestParam = routingContext.getBodyAsJson().getString("requestParam");
            URL url = new URL(HttpServerVerticle.medoxUrl + "/medoxweb/DefaultProviderAccessSettings.do" + requestParam);
            GETRequestToApiMedox GETRequestToApiMedox = new GETRequestToApiMedox();
            GETRequestToApiMedox.sendRequest(url, routingContext,true);
        } catch (MalformedURLException e) {
            routingContext.response().setStatusCode(500).end();
        }
       /* try {

            String requestParam = routingContext.getBodyAsJson().getString("requestParam");
            URL url = new URL(HttpServerVerticle.medoxUrl + "/medoxweb/DefaultProviderAccessSettings.do"+requestParam);
            List<String> listParams = new ArrayList<>();
            JsonArray params = routingContext.getBodyAsJson().getJsonArray("param");
            String cookie = routingContext.getBodyAsJson().getString("cookie");

            List<HttpCookie> cookies = HttpCookie.parse(cookie);


            for (Object list: params.getList()){
                listParams.add(((HashMap<String,String>) list).get("name"));
            }
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Cookie", StringUtils.join(cookies.get(0)));
            con.getResponseCode();

            StringBuilder response = parsingService.parse(con,listParams, cookie);
            routingContext.response().end(String.valueOf(response));
        }catch (Exception ex){

        }*/
    }
}