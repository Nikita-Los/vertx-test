package handler.medox;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;
import service.ParsingService;
import verticle.HttpServerVerticle;

import java.io.DataOutputStream;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class NewsletterSettings implements Handler<RoutingContext> {

    private ParsingService parsingService = new ParsingService();

    @Override
    public void handle(RoutingContext routingContext) {
        try {

            String cookie = routingContext.getBodyAsJson().getString("cookie");
            String PREFRENCES_ISNEWSLETTER = routingContext.getBodyAsJson().getString("PREFRENCES_ISNEWSLETTER");
            String PREFRENCES_ISTEXT = routingContext.getBodyAsJson().getString("PREFRENCES_ISTEXT");
            String action = routingContext.getBodyAsJson().getString("action");

            List<HttpCookie> cookies = HttpCookie.parse(cookie);

            URL url = new URL(HttpServerVerticle.medoxUrl + "/medoxweb/NewsletterSettings.do");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            String urlParameters = "PREFRENCES_ISNEWSLETTER=" + PREFRENCES_ISNEWSLETTER + "PREFRENCES_ISTEXT=" + PREFRENCES_ISTEXT + "&action=" + action;
            con.setRequestProperty("Cookie", StringUtils.join(cookies.get(0)));
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            con.getInputStream();

            routingContext.response().setStatusCode(200).end();

        } catch (Exception ex) {

        }
    }
}
