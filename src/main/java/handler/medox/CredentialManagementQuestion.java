package handler.medox;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import service.POSTRequestToApiMedox;
import verticle.HttpServerVerticle;

import java.net.MalformedURLException;
import java.net.URL;

public class CredentialManagementQuestion implements Handler<RoutingContext> {

    @Override
    public void handle(RoutingContext routingContext) {
        try {
            URL url = new URL(HttpServerVerticle.medoxUrl + "/medoxweb/CredentialManagement.do");
            POSTRequestToApiMedox postRequestToApiMedox = new POSTRequestToApiMedox();
            postRequestToApiMedox.sendRequest(url, routingContext, false);
        } catch (MalformedURLException e) {
            routingContext.response().setStatusCode(500).end();
        }
    }
}
