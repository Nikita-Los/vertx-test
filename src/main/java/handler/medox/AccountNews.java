package handler.medox;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import service.GETRequestToApiMedox;
import verticle.HttpServerVerticle;

import java.net.MalformedURLException;
import java.net.URL;

public class AccountNews implements Handler<RoutingContext> {

    @Override
    public void handle(RoutingContext routingContext) {
        try {
            URL url = new URL(HttpServerVerticle.medoxUrl + "/medoxweb/AccountNews.do?action=listAll");
            GETRequestToApiMedox GETRequestToApiMedox = new GETRequestToApiMedox();
            GETRequestToApiMedox.sendRequest(url, routingContext, false);
        } catch (MalformedURLException e) {
            routingContext.response().setStatusCode(500).end();
        }
    }
}
