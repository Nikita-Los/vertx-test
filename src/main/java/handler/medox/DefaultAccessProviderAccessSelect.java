package handler.medox;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;
import service.ParsingService;
import verticle.HttpServerVerticle;

import java.io.DataOutputStream;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DefaultAccessProviderAccessSelect implements Handler<RoutingContext> {

    private ParsingService parsingService = new ParsingService();

    @Override
    public void handle(RoutingContext routingContext) {
        try {

            List<String> listParams = new ArrayList<>();
            JsonArray params = routingContext.getBodyAsJson().getJsonArray("param");
            String cookie = routingContext.getBodyAsJson().getString("cookie");
            String currentAccessPolicyPanelSetName = routingContext.getBodyAsJson().getString("currentAccessPolicyPanelSetName");
            String action = routingContext.getBodyAsJson().getString("action");

            List<HttpCookie> cookies = HttpCookie.parse(cookie);

            for (Object list : params.getList()) {
                listParams.add(((HashMap<String, String>) list).get("name"));
            }

            URL url = new URL(HttpServerVerticle.medoxUrl + "/medoxweb/DefaultProviderAccessSettings.do");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            String urlParameters = "currentAccessPolicyPanelSetName=" + currentAccessPolicyPanelSetName + "&action=" + action;
            con.setRequestProperty("Cookie", StringUtils.join(cookies.get(0)));
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            StringBuilder response = parsingService.parse(con, listParams, cookie);
            routingContext.response().end(String.valueOf(response));

        } catch (Exception ex) {

        }
    }
}
