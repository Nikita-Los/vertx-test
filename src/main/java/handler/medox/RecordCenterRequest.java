package handler.medox;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import service.POSTRequestToApiMedox;
import service.ParsingService;
import verticle.HttpServerVerticle;

import java.net.MalformedURLException;
import java.net.URL;

public class RecordCenterRequest implements Handler<RoutingContext> {

    ParsingService parsingService = new ParsingService();

    @Override
    public void handle(RoutingContext routingContext) {
        try {
            URL url = new URL(HttpServerVerticle.medoxUrl + "/medoxweb/RequestRecord.do");
            POSTRequestToApiMedox postRequestToApiMedox = new POSTRequestToApiMedox();
            postRequestToApiMedox.sendRequest(url, routingContext, false);
        } catch (MalformedURLException e) {
            routingContext.response().setStatusCode(500).end();
        }
    }
}
