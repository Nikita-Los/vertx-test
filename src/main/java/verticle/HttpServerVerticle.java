package verticle;


import handler.medox.*;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.net.PemTrustOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.sstore.LocalSessionStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import run.Runner;

public class  HttpServerVerticle extends AbstractVerticle {

    public static String medoxUrl = "http://127.0.0.1:8080";

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpServerVerticle.class);

    public static void main(String[] args) {
        Runner.runExample(HttpServerVerticle.class);
    }

    @Override
    public void start(Future<Void> startFuture) {

        Router mainRouter = Router.router(vertx);

        mainRouter.route().handler(CookieHandler.create());
        mainRouter.route().handler(BodyHandler.create());
        mainRouter.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));


        mainRouter.route().handler(CorsHandler.create("*").allowedMethod(HttpMethod.GET));
        mainRouter.route().handler(CorsHandler.create("*").allowedMethod(HttpMethod.POST));


        mainRouter.post("/login_medox").handler(new LoginHandlerToMedox());
        mainRouter.post("/account_center").handler(new AccountCenter());
        mainRouter.post("/account_data").handler(new AccountData());
        mainRouter.post("/account_data_save").handler(new AccountDataSave());
        mainRouter.post("/account_status").handler(new AccountStatus());
        mainRouter.post("/user_settings").handler(new UserSettings());
        mainRouter.post("/downloads").handler(new Downloads());
        mainRouter.post("/account_news").handler(new AccountNews());
        mainRouter.post("/cred_manag").handler(new CredentialManagement());
        mainRouter.post("/cred_manag_question").handler(new CredentialManagementQuestion());
        mainRouter.post("/cred_manag_pass").handler(new AccountData());

        mainRouter.post("/record_center").handler(new RecordCenter());
        mainRouter.post("/record_center_links").handler(new RecordCenterLinks());
        mainRouter.post("/record_center_request").handler(new RecordCenterRequest());
        mainRouter.post("/record_center_manage_records").handler(new RecordCenterManageRecords());
        mainRouter.post("/generate_record").handler(new GenerateRecord());
        mainRouter.post("/organization_settings").handler(new OrganizationSettings());
        mainRouter.post("/organization_account_data").handler(new OrganizationAccountData());
        mainRouter.post("/organization_cred_manag").handler(new OrganizationCredentialManagement());
        mainRouter.post("/def_provider_access_select").handler(new DefaultAccessProviderAccessSelect());
        mainRouter.post("/def_provider_access").handler(new DefaultAccessProviderAccess());
        mainRouter.post("/newsletter_settings").handler(new NewsletterSettings());
        mainRouter.post("/notification_settings").handler(new NotificationSettings());
        mainRouter.post("/associate_adminInstitutionalProvider").handler(new AssociateInstitutionalProvider());
        mainRouter.post("/institutional_provider_search_results_step3").handler(new AssociatingInstitutionalProviderStep3());
        mainRouter.post("/institutional_provider_search_results_steps").handler(new AssociatingInstitutionalProviderSteps());
        mainRouter.post("/associate_admin_insurance_company").handler(new AssociateInsuranceCompany());
        mainRouter.post("/insurance_company_search_results_steps").handler(new AssociatingInsuranceCompanySteps());
        mainRouter.post("/insurance_company_search_results_step3").handler(new AssociatingInsuranceCompanyStep3());
        mainRouter.post("/associate_admin_employer_group").handler(new AssociateEmployerGroup());
        mainRouter.post("/employer_group_search_results_steps").handler(new AssociatingEmployerGroupSteps());
        mainRouter.post("/employer_group_search_results_step3").handler(new AssociatingEmployerGroupStep3());
        mainRouter.post("/get_access_settings_wthn_policy").handler(new PolicyAccessSettings());


        mainRouter.post("/associate_adminWithInstitutionalProvider").handler(new AssociateWithInstitutionalProvider());
        mainRouter.post("/associate_with_institutional_provider_results_steps").handler(new AssociatingInsuranceCompanySteps());
        mainRouter.post("/associate_with_institutional_provider_results_step3").handler(new AssociatingInsuranceCompanyStep3());


        createAndStartServer(startFuture, mainRouter);
    }

    private void createAndStartServer(Future<Void> startFuture, Router router) {
        HttpServer server = vertx.createHttpServer(new HttpServerOptions()
                .setSsl(false)
                .setPemTrustOptions(new PemTrustOptions().addCertPath("myCA.key"))
        );
        int portNumber = 8082;
        server
                .requestHandler(router::accept)
                .listen(portNumber, ar -> {
                    if (ar.succeeded()) {
                        LOGGER.info("HTTP server running on port " + portNumber);
                        startFuture.complete();
                    } else {
                        LOGGER.error("Could not start a HTTP server", ar.cause());
                        startFuture.fail(ar.cause());
                    }
                });
    }
}
