package service;

import io.vertx.core.MultiMap;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;
import verticle.HttpServerVerticle;

import java.io.DataOutputStream;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class POSTRequestToApiMedox {

    private ParsingService parsingService = new ParsingService();

    public void sendRequest(URL url, RoutingContext routingContext,Boolean responseIsNeeded) {
        try {
            List<String> listParams = new ArrayList<>();
            String urlParams = "";
            String cookie = "";

            MultiMap paramMap = routingContext.request().formAttributes();

            Iterator iterator = paramMap.entries().iterator();

            while (iterator.hasNext()) {
                Map.Entry entry = (Map.Entry) iterator.next();
                if (entry.getKey().equals("cookie")){
                    cookie = (String) entry.getValue();
                }
                else if (entry.getKey().equals("param_names")){
                    listParams = Stream.of(((String) entry.getValue()).split(",")).collect(Collectors.toList());
                }else {
                    urlParams += entry.getKey() + "=" + entry.getValue();
                    if (iterator.hasNext()) {
                        urlParams += "&";
                    }
                }
            }

            if (urlParams.endsWith("&")){
                urlParams = urlParams.substring(0,urlParams.length()-1);
            }

            byte[] postData = urlParams.getBytes( StandardCharsets.UTF_8 );
            int postDataLength = postData.length;

            List<HttpCookie> cookies = HttpCookie.parse(cookie);

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setInstanceFollowRedirects(false);
            con.setRequestMethod("POST");
            con.setRequestProperty("Cookie", StringUtils.join(cookies.get(0)));
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("charset", "utf-8");
            con.setRequestProperty("Content-Length", Integer.toString(postDataLength ));
            con.setUseCaches(false);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.write( postData );
            wr.flush();
            wr.close();

            con.getInputStream();
            int responseCode = con.getResponseCode();

            if (responseCode==500){
                routingContext.response().setStatusCode(401).end();
            }
            if (responseIsNeeded){
                StringBuilder response = parsingService.parse(con,listParams, cookie);
                routingContext.response().end(String.valueOf(response));
            }else {
                routingContext.response().setStatusCode(200).end();
            }
        }catch (Exception ex){
            routingContext.response().setStatusCode(500).end();
        }
    }
}
