package service;

import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;

import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GETRequestToApiMedox {

    private ParsingService parsingService = new ParsingService();

    public void sendRequest(URL url,RoutingContext routingContext,Boolean responseIsNeeded){
        try {
            List<String> listParams = new ArrayList<>();
            JsonArray params = routingContext.getBodyAsJson().getJsonArray("param");
            String cookie = routingContext.getBodyAsJson().getString("cookie");

            List<HttpCookie> cookies = HttpCookie.parse(cookie);


            for (Object list : params.getList()) {
                listParams.add(((HashMap<String, String>) list).get("name"));
            }
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Cookie", StringUtils.join(cookies.get(0)));
            con.getResponseCode();

            int responseCode = con.getResponseCode();
            if (responseCode==500){
                routingContext.response().setStatusCode(401).end();
            }

            if (responseIsNeeded){
                StringBuilder response = parsingService.parse(con,listParams, cookie);
                routingContext.response().end(String.valueOf(response));
            }else {
                routingContext.response().setStatusCode(200).end();
            }
        }
        catch (Exception ex){
            routingContext.response().setStatusCode(500).end();
        }
    }
}
