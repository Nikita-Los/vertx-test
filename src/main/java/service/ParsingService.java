package service;

import java.io.*;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ParsingService {

    public StringBuilder parse(HttpURLConnection con, List<String> listParams, String cookieString) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStream is = new BufferedInputStream(con .getInputStream());
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String inputLine;
        while ((inputLine = br.readLine()) != null) {
            sb.append(inputLine);
        }
        String result = sb.toString();

        /*if (result.contains("Solutions") || result.isEmpty()){
            routingContext.response().end(String.valueOf(response));
        }9*/


        HashMap<String,String> mapJson = new HashMap<>();

        while (result.contains("beginJsonDocument")) {
            int start = result.indexOf("beginJsonDocument");
            int end = result.indexOf("endJsonDocument");
            String jsonWithKey = result.substring(start + 17, end);
            String stringToReplace = result.substring(start, end+15);
            int endKey = jsonWithKey.indexOf("\"");
            if(endKey == -1){
                result = result.replace(stringToReplace,"");
                continue;
            }
            String key = jsonWithKey.substring(1,endKey);
            String json = jsonWithKey.substring(endKey);
            mapJson.put(key,"{"+json+"}");
            result = result.replace(stringToReplace,"");
        }


        StringBuilder response = new StringBuilder("[");
        for (Object listParam : listParams) {
            String str = mapJson.get(listParam);
            if (str != null) {
                response.append(str);
                response.append(",");
            }
        }
        response.append("{\"cookie\":\"").append(cookieString).append("\"}");
        response.append("]");
        return response;
    }
}
